import { ChainId } from '@encodix/sdk-ava'
import MULTICALL_ABI from './abi.json'

const MULTICALL_NETWORKS: { [chainId in ChainId]: string } = {
  [ChainId.AVALANCHE]: '0xa1B2b503959aedD81512C37e9dce48164ec6a94d',
  [ChainId.FUJI]: '0x301907b5835a2d723Fe3e9E8C5Bc5375d5c1236A'
}

export { MULTICALL_ABI, MULTICALL_NETWORKS }
